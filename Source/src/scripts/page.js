(function() {  

    $(document).ready(function(){

        var currPage = 1;
        var pageSize = 10;
        var $table = $('table');
        var $rows = $table.find('tbody tr');
        var rows = $rows.length;
        var totalPages = rows/pageSize;
    $('div[name="t-id"]').text(totalPages);

        function showPage(pageNumber){
            $rows.hide();
            var cal = (pageNumber*pageSize);
            var endIndex = cal;
            var startIndex = cal-pageSize;
            $rows.slice(startIndex , endIndex).show();
            currPage = pageNumber;
            $('div[name="pg-id"]').text(pageNumber);
        }

        $('#next').on('click', function(){
            if(currPage === totalPages){
                return ;
            }
            showPage(currPage+1);
        });
        $('#prev').on('click', function(){
            if(currPage === 1){
                return ;
            }
            showPage(currPage-1);
        });

        showPage(1);


    });

})();