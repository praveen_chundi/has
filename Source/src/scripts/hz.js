(function() {
    $(document).ready(function() {

        var emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var re;
        ///^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{8,15}$/;
        function updateErrorView($object, isValid, message) {

            var $parent = $object.parent('div.form-group');
            if (isValid) {
                if ($parent.hasClass('has-error')) {
                    $parent.removeClass('has-error');
                    $parent.find('.help-block').addClass('hide');
                }
            } else {
                $parent.addClass('has-error');
                var $helpBlock = $parent.find('.help-block');
                $helpBlock.removeClass('hide');
                if (typeof message !== 'undefined') {
                    $helpBlock.html(message);
                }
            };
        }
        //$form.submit(function(e){

        $('body').on('submit', 'form', function(e) {
            e.preventDefault();
            var $form = $(this);
            var $firstName = $form.find('input[name=firstname]');
            var $lastName = $form.find('input[name=lastname]');
            var $email = $form.find('input[name=email]');
            var $password = $form.find('input[name=password]');
            var $cpassword = $form.find('input[name=confirm_password]');
            var $phone = $form.find('input[name=phone]');
            updateErrorView($firstName, !($firstName.val() === ''));
            updateErrorView($lastName, !($lastName.val() === ''));
            if ($email.val() === '') {
                updateErrorView($email, !($email.val() === ''), "Email is required");
            } else {
                updateErrorView($email, (emailRegex.test($email.val())), "Invalid Email address");
            }
            if ($password.val() === '' && $cpassword.val() === '') {
                updateErrorView($password, !($password.val() === ''));
                updateErrorView($cpassword, !($cpassword.val() === ''));
            }

            //The passwords don't match. Try again?
            if ($password.val() !== $cpassword.val()) {
                updateErrorView($cpassword, ($cpassword.val() === ''), "The passwords don't match. Try again?");
            }
            //$password.val()!=='' && $cpassword.val()!==''&&
            if ($password.val() === $cpassword.val()) {
                var passwordErrorMsg = "";
                var isPasswordValid = true;
                updateErrorView($password, !($password.val() === ''));
                updateErrorView($cpassword, !($cpassword.val() === ''));
                //console.log("match");
                if ($password.val().length < 8) {
                    isPasswordValid = false;
                    passwordErrorMsg = "Error: Password must contain at least eight characters! <br/>";
                    //updateErrorView($password, !($password.val() === ''), "Password must contain atleast eight characters");
                }
                re = /[0-9]/;
                if (!re.test($password.val())) {
                    isPasswordValid = false;
                    passwordErrorMsg = passwordErrorMsg + "Error: Password must contain at least one number! <br/>";
                    //updateErrorView($password, !($password.val() === ''), "Password must contain atleast one number");
                }
                re = /[A-Z]/;
                if (!re.test($password.val())) {
                    isPasswordValid = false;
                    passwordErrorMsg = passwordErrorMsg + "Error: Password must contain at least one uppercase characters! <br/>";
                    //updateErrorView($password, !($password.val() === ''), "Password must contain atleast one uppercase letter");
                }
                re = /[a-z]/;
                if (!re.test($password.val())) {
                    isPasswordValid = false;
                    passwordErrorMsg = passwordErrorMsg + "Error: Password must contain at least one lowercase characters!";
                    //updateErrorView($password, !($password.val() === ''), "Password must contain atleast one lowercase letter");
                }

                if (!isPasswordValid) {
                    updateErrorView($cpassword, isPasswordValid, passwordErrorMsg);
                }
                // else
                //     return true;
            }

            updateErrorView($phone, !($phone.val() === ''));

        });

    });

})();

function sort1() {
    var arr = ['c', 'b', 'a'];
    var arrLen = arr.length;
    for (var i = 1; i < arrLen; i++) {
        for (var k = i; k >= 0; k--) {
            var curVal = arr[k];
            var j = k - 1;
            var idxVal = arr[j];
            if (curVal < idxVal) {
                arr[j] = curVal;
                arr[k] = idxVal;

            } else {
                break;
            }

        }
    }
    console.log(arr);
}