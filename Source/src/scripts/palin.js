(function() {
    $(document).ready(function() {
        var obj1 = {
            SNo: 1,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj2 = {
            SNo: 2,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj3 = {
            SNo: 3,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj4 = {
            SNo: 4,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj5 = {
            SNo: 5,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj6 = {
            SNo: 6,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj7 = {
            SNo: 7,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj8 = {
            SNo: 8,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj9 = {
            SNo: 9,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj10 = {
            SNo: 10,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj11 = {
            SNo: 11,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj12 = {
            SNo: 12,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj13 = {
            SNo: 13,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj14 = {
            SNo: 14,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj15 = {
            SNo: 15,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj16 = {
            SNo: 16,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj17 = {
            SNo: 17,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj18 = {
            SNo: 18,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj19 = {
            SNo: 19,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var obj20 = {
            SNo: 20,
            Fname: "Praveen",
            Lname: "Chundi",
            age: 23,
            country: "India"
        };
        var array = [obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15, obj16, obj17, obj18, obj19, obj20];

        function tbl(arr) {
            var tb = $('table tbody');
            var $tbody = $("<tbody>");
            var arrLen = arr.length;
            for (i = 0; i < arrLen; i++) {
                var $tr = $("<tr>");
                var obj = arr[i];
                $.each(obj, function(key, val) {

                    $tr.append('<td>' + val + '</td>');

                })

                $tbody.append($tr);
            }

            tb.html($tbody.html());
        }
        tbl(array);
        var currPage = 1;
        var pageSize = 5;
        var $table = $('table');
        var $rows = $table.find('tbody tr');
        var rows = $rows.length;
        var totalPages = rows/pageSize;
        for(i=1;i<=totalPages;i++){
            $('div[name="pg-id"]').append('<a href="#" rel="'+i+'">'+i+'</a> ');
        }

    $('div[name="t-id"]').text(totalPages);
			function showPage(pageNumber){
            $rows.hide();
            var cal = (pageNumber*pageSize);
            var endIndex = cal;
            var startIndex = cal-pageSize;
            $rows.slice(startIndex , endIndex).show();
            currPage = pageNumber;
        }
///////////////////////////////
                $('div[name="pg-id"] a').bind('click', function(e){
                e.preventDefault();
                $('div[name="pg-id"] a').removeClass('active');
                $(this).addClass('active');
                currPage = $(this).attr('rel');
                showPage(currPage);
        });
            
        ////////////////
        $('#next').on('click', function(){
            if(currPage === totalPages){
                return ;
            }
            showPage(currPage+1);
        });
        $('#prev').on('click', function(){
            if(currPage === 1){
                return ;
            }
            showPage(currPage-1);
        });
        showPage(1);

    });
})();